package com.parse.starter.config;

import com.google.android.gms.maps.model.LatLng;

public class ValetBean {
    private String spaceId;
    private String spaceName;
    private String spaceCount;
    private LatLng latLng;
    private String spaceRate;
    private String valetCount;

    public String getSpaceCount() {
        return spaceCount;
    }

    public void setSpaceCount(String spaceCount) {
        this.spaceCount = spaceCount;
    }

    public String getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(String spaceId) {
        this.spaceId = spaceId;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getSpaceRate() {
        return spaceRate;
    }

    public void setSpaceRate(String spaceRate) {
        this.spaceRate = spaceRate;
    }

    public String getValetCount() {
        return valetCount;
    }

    public void setValetCount(String valetCount) {
        this.valetCount = valetCount;
    }

}