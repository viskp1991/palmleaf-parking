package com.parse.starter.config;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.Locale;

public class MyLocation implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;
    private Location bestLocaton = null;
    public Context mContext;
    private LocationResult locationResult;
    private String address = "";
    private int timeInMills;

    public MyLocation(Context context, int timeInMills) {
        mContext = context;
        this.timeInMills = timeInMills;
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        // TODO Auto-generated constructor stub

    }

    public void start() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    public void stop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult location) {
        // TODO Auto-generated method stub
        locationResult.locationFailed(location);
        locationResult.locationNotAvailable();

    }

    @Override
    public void onConnected(Bundle location) {
        // TODO Auto-generated method stub
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(timeInMills); // Update location every Minute
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int location) {
        // TODO Auto-generated method stub
        locationResult.locationSuspended(location);
        locationResult.locationNotAvailable();

    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub

        bestLocaton = location;
        locationResult.gotLocation(location, timeInMills);

        locationResult.gotLocation(bestLocaton, timeInMills);

        // Passing Location Address

        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        Double lat = location.getLatitude();
        Double lng = location.getLongitude();
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            address = addresses.get(0).getAddressLine(1);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        locationResult.gotLocationAddress(bestLocaton, address);
        stop();

    }

    public boolean initialize(LocationResult _locationResult) {

        locationResult = _locationResult;
        start();
        return false;

    }

    public static interface LocationResult {
        public abstract void gotLocation(Location location, int timeInMils);

        public abstract void gotLocationAddress(Location location,
                                                String address);

        public abstract void locationNotAvailable();

        public abstract void locationFailed(ConnectionResult connectionResult);


        public abstract void locationSuspended(int location);
    }

    /**
     * Check weather location is turned on or not
     * @return
     */
    public boolean isLocationEnable() {
        boolean status = true;
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                && !locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // All location services are disabled
            status = false;
        }
        return status;
    }
}