package com.parse.starter.UserAuth;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.parse.starter.R;
import com.parse.starter.pojo.RegisterBean;
import com.parse.starter.utils.GlobalUtils;

import static com.parse.starter.utils.GlobalUtils.REGISTRATION_TABLE;

public class RegisterActivity extends AppCompatActivity {

    private EditText emailField, passwrodField, nameField;
    private ProgressDialog dialog;
    private Context activity;
    private String passwordText;
    private String nameText;
    private String emailID;
    private boolean isDataThere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        setContentView(R.layout.activity_register);
        dialog = new ProgressDialog(RegisterActivity.this);
        dialog.setCancelable(false);
        emailField = (EditText) findViewById(R.id.emailField);
        passwrodField = (EditText) findViewById(R.id.passwrodField);
        nameField = (EditText) findViewById(R.id.nameField);
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRegisterStart();
            }
        });
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancel();
            }
        });
    }

    public void onRegisterStart() {
        showLoading("Signing up...");
        passwordText = passwrodField.getText().toString();
        nameText = nameField.getText().toString();
        emailID = emailField.getText().toString();
        if (isValidEmail(emailID) && passwordText.length() > 6 && nameText.length() > 4) {
            isUserExists();
        } else {
            hideProgressDialog();
            Toast.makeText(this, "Please enter valid Email Id, Password and Name.", Toast.LENGTH_SHORT).show();
        }
    }

    private void isUserExists() {
        isDataThere = false;
        DatabaseReference newRef = FirebaseDatabase.getInstance().getReference(REGISTRATION_TABLE);
        newRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop:
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    RegisterBean post = child.getValue(RegisterBean.class);
                    if (post.getEmail().equals(emailID)) {
                        isDataThere = true;
                        break;
                    } else {
                        isDataThere = false;
                    }
                }
                if (isDataThere)
                    Toast.makeText(activity, "email exists", Toast.LENGTH_SHORT).show();
                else
                    registerUser();
                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void onCancel() {
        finish();
    }

    private void registerUser() {
        RegisterBean user = new RegisterBean();
        user.setUserName(nameText);
        user.setPassword(passwordText);
        user.setEmail(emailID);
        DatabaseReference newRef = GlobalUtils.getRegistrationTableForPush();
        user.setUserID(newRef.getKey());
        newRef.setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(RegisterActivity.this, "Log In", Toast.LENGTH_SHORT).show();
                    Intent homeIntent = new Intent(RegisterActivity.this, AuthActivity.class);
                    startActivity(homeIntent);
                    finish();
                }
                hideProgressDialog();
            }
        });

    }

    public boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void showLoading(String message) {
        dialog.setMessage(message);
        if ((dialog != null))
            dialog.show();
    }

    private void hideProgressDialog() {
        if ((dialog != null) && (dialog.isShowing())) {
            dialog.dismiss();
        }
    }
}
