package com.parse.starter.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Vishal.Pandey on 9/11/2017.
 */

public class GlobalUtils {
    public static String REGISTRATION_TABLE = "registration";
    public static String PARKING_INFO_TABLE = "parking_info";
    public static String PREF = "pref";
    public static String IS_LOGGED_IN = "isLoggedIn";

    public static DatabaseReference getRegistrationTableForPush() {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference newRef = myRef.child(REGISTRATION_TABLE).push();
        return newRef;
    }

    public static void setUserLogin(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF, Context.MODE_PRIVATE).edit();
        editor.putBoolean(IS_LOGGED_IN, true).commit();
    }

    public static void setUserLogout(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF, Context.MODE_PRIVATE).edit();
        editor.putBoolean(IS_LOGGED_IN, false).commit();
    }

    public static boolean isUserLoggedIn(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return preferences.getBoolean(IS_LOGGED_IN, false);
    }
}
