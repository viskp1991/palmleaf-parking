package com.parse.starter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.List;

public class ParkingSpaceActivity extends AppCompatActivity {
    private TextView totalTimeTV, priceTV;
    private Button doneButton;
    private String parkSpaceID;
    private String parkItemID;
    private ParseObject parkSpace;
    private ParseObject parkItem;
    private int rate;
    private TextView loadingTV;
    //    private LocationManager locationManager;
//    private MyLocationListener myLocationListener;
    private Location userLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_space);
        loadingTV = (TextView) findViewById(R.id.loading);
        parkSpaceID = getIntent().getStringExtra("parkSpaceID");
        parkItemID = getIntent().getStringExtra("parkItemID");
        totalTimeTV = (TextView) findViewById(R.id.totalTime);
        priceTV = (TextView) findViewById(R.id.price);
        doneButton = (Button) findViewById(R.id.done);
        loadingTV.setText("You parked at: \n" + getIntent().getStringExtra("parkSpaceName") + " \n Start Time: " + getIntent().getSerializableExtra("date"));
//        myLocationListener = new MyLocationListener();
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Thanks!!!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateEndTime();
            }
        }, 10000);
    }

//    public final class MyLocationListener implements LocationListener {
//        @Override
//        public void onLocationChanged(Location location) {
//            calculateIsUserInsideParkingArea(location);
//        }
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//
//        }
//
//        @Override
//        public void onProviderEnabled(String provider) {
//
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {
//
//        }
//    }

//    private void calculateIsUserInsideParkingArea(Location location) {
//        if (checkRadius(location)) {
//            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                return;
//            }
////            locationManager.removeUpdates(myLocationListener);
//            endUserParkingAtParkSpace(parkSpace, parkItem);
//        }
//    }

    private void updateEndTime() {
        ParseQuery parseQuery = new ParseQuery("ParkSpace");
        parseQuery.whereEqualTo("objectId", parkSpaceID);
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (objects != null) {
                    parkSpace = objects.get(0);
                    ParseGeoPoint parseGeoPoint = parkSpace.getParseGeoPoint("Location");
                    userLocation = new Location(LocationManager.GPS_PROVIDER);
                    userLocation.setLatitude(parseGeoPoint.getLatitude());
                    userLocation.setLongitude(parseGeoPoint.getLongitude());
                    rate = (int) parkSpace.getNumber("Rate");
                    ParseQuery parseQuery = new ParseQuery("ParkItem");
                    parseQuery.whereEqualTo("objectId", parkItemID);
                    parseQuery.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {
                            parkItem = objects.get(0);
                            endUserParkingAtParkSpace(parkSpace, parkItem);
//                            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                            if (ActivityCompat.checkSelfPermission(ParkingSpaceActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ParkingSpaceActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                                return;
//                            }
//                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10 * 1000, 0, myLocationListener);
                        }

                    });
                } else {
                    Toast.makeText(getApplicationContext(), "Waiting...", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

        });
    }

    public void endUserParkingAtParkSpace(ParseObject parkSpace, final ParseObject parkItem) {
        parkSpace.put("SpaceCount", parkSpace.getInt("SpaceCount") + 1);
        parkSpace.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

            }
        });

        parkItem.put("EndTime", Calendar.getInstance().getTime());
        parkItem.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(ParkingSpaceActivity.this, "Your Time Ended Successfully", Toast.LENGTH_SHORT).show();
                    totalTimeTV.setText("Total Time Taken: " + parkItem.getDate("StartTime") + parkItem.getDate("EndTime"));
                    priceTV.setText("Total Fare: \u20B9" + rate);
                    loadingTV.setVisibility(View.GONE);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean checkRadius(Location location) {
        float distance = userLocation.distanceTo(location); // distance in meters
        if (distance > 1000) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
