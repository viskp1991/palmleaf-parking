/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.parse.FindCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.starter.config.MyLocation;
import com.parse.starter.config.PathJSONParser;
import com.parse.starter.config.ValetBean;
import com.parse.starter.pojo.ParkingSpaceInfoPojo;
import com.parse.starter.utils.GlobalUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Timer;

import static com.parse.starter.utils.GlobalUtils.PARKING_INFO_TABLE;


public class MainActivity extends ActionBarActivity implements MyLocation.LocationResult, OnMapReadyCallback {
    private static final int VALET_TIME = 1000 * 60 * 5;
    private static final int CHECK_PARK_TIME = 1000 * 3;
    private ImageView navDrawer;
    private Context context;
    private GoogleMap mMap;
    public DrawerLayout drawerLayout;
    public boolean isDrawerClosed;
    public ActionBarDrawerToggle drawerToggle;
    public View drawerView;
    private MyLocation mLastLocation;
    private AlertDialog.Builder locationDialog;
    private AlertDialog.Builder internetDialog;
    private AlertDialog alertDialog;
    private ListView listView;
    private static final int reqCode = 100;
    private TextView userName, userMob;
    private Timer myTimer;
    private ArrayList<LatLng> srDestLatLngs;
    private ProgressDialog dialog;
    private LinkedHashMap<LatLng, ParkingSpaceInfoPojo> users;
    private ArrayList<LatLng> latLngArr;
    private Location clickedLocation;
    private String clickedParseObjectId;
    private LocationManager locationManager;
    //    private MyLocationListener myLocationListener;
    private String parkSpaceID;
    private boolean isShowLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        dialog = new ProgressDialog(MainActivity.this);
        dialog.setCancelable(false);
//        myLocationListener = new MyLocationListener();
        userName = (TextView) findViewById(R.id.user_name);
        userMob = (TextView) findViewById(R.id.user_mob);
        listView = (ListView) findViewById(R.id.list_view);
        navDrawer = (ImageView) findViewById(R.id.action_bar_menu);
        drawerView = (View) findViewById(R.id.view);
        locationDialog = new AlertDialog.Builder(MainActivity.this);
        locationDialog.setIcon(R.drawable.ic_launcher);
        locationDialog.setCancelable(false);
        locationDialog.setMessage("Location is Disabled");
        locationDialog.setMessage("Please turn on location to high accuracy for best result!");
        locationDialog.setPositiveButton("ENABLE GPS",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(viewIntent);
                        // locationDialog.create().dismiss();
                    }
                });
        locationDialog.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        finish();
                    }
                });
        alertDialog = locationDialog.create();
        mLastLocation = new MyLocation(context, VALET_TIME);
        mLastLocation.initialize(this);
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        String[] values = new String[]{"Satellite", "Terrain", "logout"};
        final ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }
        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        break;
                    case 1:
                        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                        break;
                    case 2:
                        GlobalUtils.setUserLogout(context);
                        Toast.makeText(context, "Logged out!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(context, Splash.class));
                        break;
                }
                drawerLayout.closeDrawer(drawerView);
            }
        });
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout,
                R.drawable.actionbar_menu, R.string.hello_world,
                R.string.hello_world) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                supportInvalidateOptionsMenu();
                isDrawerClosed = true;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                supportInvalidateOptionsMenu();
                isDrawerClosed = false;
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        navDrawer.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isDrawerClosed) {
                    isDrawerClosed = false;
                    drawerLayout.closeDrawer(drawerView);
                } else {
                    drawerLayout.openDrawer(drawerView);
                    isDrawerClosed = true;
                }
                return false;
            }
        });
        onNewIntent(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkPlayServices()) {
            if (!mLastLocation.isLocationEnable())
                showlocationDialog();
        }
        setUpMapIfNeeded();
        // execute in every 500

    }

    public boolean isConnectedToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }


    private void showlocationDialog() {
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    private void showInternetDialog() {
        internetDialog = new AlertDialog.Builder(MainActivity.this);
        internetDialog.setIcon(R.drawable.ic_launcher);
        internetDialog.setCancelable(false);
        internetDialog.setTitle("Connection Error!");
        internetDialog.setMessage("Check Your Internet Connection");
        internetDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        internetDialog.show();
    }

    private void setUpMapIfNeeded() {
        if (mMap != null) {
            return;
        }
        ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMapAsync(this);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        100).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }


//    private void calculateIsUserInsideParkingArea(Location location) {
//        if (checkRadius(location)) {
//            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                return;
//            }
//            locationManager.removeUpdates(myLocationListener);
//            startUserParkingAtParkSpace(clickedParseObjectId);
//        }
//    }

    @Override
    public void gotLocation(Location location, int timeInMils) {
        if (location != null) {
            if (isConnectedToInternet()) {
                showLoading("Getting Spaces...");
                getParkingSpaceInfo(location.getLatitude(), location.getLongitude());
                mLastLocation.stop();
            } else
                showInternetDialog();
        } else Toast.makeText(context, "Error in getting data", Toast.LENGTH_SHORT).show();
    }

    private boolean checkRadius(Location location) {
        float distance = clickedLocation.distanceTo(location); // distance in meters
        if (distance < 1000) {
            return true;
        }
        return false;
    }

    @Override
    public void gotLocationAddress(Location location, String address) {

    }

    @Override
    public void locationNotAvailable() {

    }

    @Override
    public void locationFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void locationSuspended(int location) {

    }

    private static void zoomToCoverAllMarkers(ArrayList<LatLng> latLngList,
                                              GoogleMap map) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng marker : latLngList) {
            builder.include(marker);
        }
        LatLngBounds bounds = builder.build();
        int padding = 20; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.moveCamera(cu);
        map.animateCamera(cu);

    }

    private String getMapsApiDirectionsUrl(LatLng src, LatLng dest) {
        String origin = "origin=" + src.latitude + "," + src.longitude + "&destination=" + dest.latitude + ","
                + dest.longitude + "&";
        String waypoints = "waypoints=optimize:true|"
                + src.latitude + "," + src.longitude
                + "|" + dest.latitude + ","
                + dest.longitude;

        String sensor = "sensor=false";
        String params = origin /*+ waypoints*/ + "&" + sensor;
        String output = "json";
        String url = output + "?" + params;
        return url;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                ParkingSpaceInfoPojo valetBean = users.get(marker.getPosition());
                clickedLocation = new Location(LocationManager.GPS_PROVIDER);
                clickedLocation.setLatitude(valetBean.getLatLng().latitude);
                clickedLocation.setLongitude(valetBean.getLatLng().longitude);
                clickedParseObjectId = valetBean.getUserId();
                View valetDialogView = getLayoutInflater().inflate(R.layout.space_info, null);
                ((TextView) valetDialogView.findViewById(R.id.spaceName)).setText("Name: " + valetBean.getSpaceName());
                ((TextView) valetDialogView.findViewById(R.id.spaceCount)).setText("Space Count: " + valetBean.getSpaceCount());
                ((TextView) valetDialogView.findViewById(R.id.valetCount)).setText("Valet Count: " + valetBean.getValetCount());
                ((TextView) valetDialogView.findViewById(R.id.rate)).setText("Rate: \u20B9" + valetBean.getRate() + " per hour");
                final AlertDialog deleteDialog = new AlertDialog.Builder(MainActivity.this).create();
                deleteDialog.setView(valetDialogView);
                valetDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteDialog.dismiss();
                    }
                });
                valetDialogView.findViewById(R.id.request).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mLastLocation.stop();
                        showLoading("Wait...");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getBestLocation();
                                deleteDialog.dismiss();
                            }
                        }, 5000);
                    }
                });

                deleteDialog.show();
                return false;
            }
        });
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }
                polyLineOptions.addAll(points);
                polyLineOptions.width(5);
                polyLineOptions.color(Color.GREEN);
            }
            mMap.addPolyline(polyLineOptions);
            zoomToCoverAllMarkers(srDestLatLngs, mMap);

        }
    }

    private void showLoading(String message) {
        dialog.setMessage(message);
        if ((dialog != null))
            dialog.show();
    }

    private void hideProgressDialog() {
        if ((dialog != null) && (dialog.isShowing())) {
            dialog.dismiss();
        }
    }

    private boolean isLoadingVisible() {
        return ((dialog != null) && (dialog.isShowing())) ? true : false;
    }

    private void getParkingSpaceInfo(double latitude, double longitude) {
//    28.432176, 77.014654 : Comviva
        users = new LinkedHashMap<>();
        latLngArr = new ArrayList<>();
        DatabaseReference newRef = FirebaseDatabase.getInstance().getReference(PARKING_INFO_TABLE);
        newRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    ParkingSpaceInfoPojo info = child.getValue(ParkingSpaceInfoPojo.class);
                    String[] latLngData = info.getLocation().split(",");
                    LatLng latLng = new LatLng(Double.parseDouble(latLngData[0]), Double.parseDouble(latLngData[1]));
                    latLngArr.add(latLng);
                    info.setLatLng(latLng);
                    users.put(latLng, info);
                }
                drawMap();
                isShowLoading = true;
                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
            }
        });
        if (isShowLoading) {
            isShowLoading = false;
        }
    }

    public void startUserParkingAtParkSpace(String id) {
        ParseQuery parseQuery = new ParseQuery("ParkSpace");
        parseQuery.whereEqualTo("objectId", id);
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List objects, ParseException e) {
                final ParseObject parseObject = (ParseObject) objects.get(0);
                ParseUser parseUser = ParseUser.getCurrentUser();
                parseObject.put("SpaceCount", parseObject.getInt("SpaceCount") - 1);
                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                    }
                });
                final ParseObject parkItem = new ParseObject("ParkItem");
                parkItem.put("UserId", parseUser.getObjectId());
                parkItem.put("ParkId", parseObject.getObjectId());
                final Date date = Calendar.getInstance().getTime();
                parkItem.put("StartTime", date);
                Log.i("TIME", Calendar.getInstance().getTime() + "");
                parkSpaceID = parseObject.getObjectId();
                parkItem.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        Toast.makeText(MainActivity.this, "Your Time Started For Parking Space", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), ParkingSpaceActivity.class);
                        intent.putExtra("parkSpaceID", parkSpaceID);
                        intent.putExtra("parkItemID", parkItem.getObjectId());
                        intent.putExtra("date", date);
                        intent.putExtra("parkSpaceName", parseObject.getString("SpaceName"));
                        hideProgressDialog();
                        startActivity(intent);
                    }
                });
            }
        });
        // save above park item object for later state when user will exit that parking

    }

    private void drawMap() {
        if (users.size() > 0) {
            ArrayList<LatLng> latLngs = new ArrayList<>();
            for (int i = 0; i < users.size(); i++) {
                ParkingSpaceInfoPojo valetInfo = users.get(latLngArr.get(i));
                if (valetInfo.getLatLng() != null) {
                    LatLng latLng = valetInfo.getLatLng();
                    latLngs.add(latLng);
                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.curr_loc)));
                }
            }
            if (latLngs.size() > 0) {
                zoomToCoverAllMarkers(latLngs, mMap);
            } else {
                Toast.makeText(context, "No near by valets", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "No near by valets", Toast.LENGTH_SHORT).show();
        }
    }

    private void getBestLocation() {
        startUserParkingAtParkSpace(clickedParseObjectId);
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10 * 1000, 0, myLocationListener);
    }

//    public final class MyLocationListener implements LocationListener {
//        @Override
//        public void onLocationChanged(Location location) {
//            calculateIsUserInsideParkingArea(location);
//        }
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//
//        }
//
//        @Override
//        public void onProviderEnabled(String provider) {
//
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {
//
//        }
//    }

}
