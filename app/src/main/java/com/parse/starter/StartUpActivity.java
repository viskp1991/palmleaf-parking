package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.parse.ParseUser;
import com.parse.starter.UserAuth.AuthActivity;

public class StartUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        ParseUser parseUser = ParseUser.getCurrentUser();
        if(parseUser == null || parseUser.getObjectId() == null){
            Intent loginIntent = new Intent(this, AuthActivity.class);
            startActivity(loginIntent);
        }else{
            Intent mainIntent = new Intent(this, MainActivity.class);
            startActivity(mainIntent);
        }

//        finish();

    }
}
