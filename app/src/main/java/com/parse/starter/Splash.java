package com.parse.starter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseUser;
import com.parse.starter.UserAuth.AuthActivity;
import com.parse.starter.UserAuth.RegisterActivity;
import com.parse.starter.utils.GlobalUtils;

public class Splash extends ActionBarActivity {
    private LinearLayout footerLL;
    private TextView signUp, login;
    private Context context;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = Splash.this;
        setContentView(R.layout.activity_splash);
        bindIds();
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, RegisterActivity.class));
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AuthActivity.class));
//                finish();
            }
        });
        progressBar.getIndeterminateDrawable().setColorFilter((Color.WHITE),
                android.graphics.PorterDuff.Mode.SRC_IN);
        if (GlobalUtils.isUserLoggedIn(context)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }
            }, 3000);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    showAnim();
                }
            }, 3000);
        }
    }

    private void bindIds() {
        signUp = (TextView) findViewById(R.id.sign_up);
        login = (TextView) findViewById(R.id.login);
        footerLL = (LinearLayout) findViewById(R.id.footer);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    private void showAnim() {
        Animation up = AnimationUtils.loadAnimation(
                getApplicationContext(), R.anim.anim_translate);
        footerLL.startAnimation(up);
        footerLL.setVisibility(View.VISIBLE);
    }

}
