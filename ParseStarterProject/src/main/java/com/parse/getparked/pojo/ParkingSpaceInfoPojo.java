package com.parse.getparked.pojo;

import com.parse.getparked.utils.GetParkedGlobalUtils;

/**
 * Created by Vishal.Pandey on 9/12/2017.
 */

public class ParkingSpaceInfoPojo {
    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setSpaceCount(int spaceCount) {
        this.spaceCount = spaceCount;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setValetCount(int valetCount) {
        this.valetCount = valetCount;
    }

    public void setValetArea(int valetArea) {
        this.valetArea = valetArea;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private String spaceName;
    private double rate;
    private int spaceCount;
    private int area;
    private int valetCount;
    private int valetArea;
    private String userId;
    private String location;

    public String getSpaceName() {
        return spaceName;
    }

    public double getRate() {
        return rate;
    }

    public int getSpaceCount() {
        return spaceCount;
    }

    public int getArea() {
        return area;
    }

    public int getValetCount() {
        return valetCount;
    }

    public int getValetArea() {
        return valetArea;
    }

    public String getUserId() {
        return userId;
    }

    public String getLocation() {
        return location;
    }
}
