package com.parse.getparked.UserAuth;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.getparked.MainActivity;
import com.parse.getparked.R;
import com.parse.getparked.pojo.GetParkedRegisterBean;
import com.parse.getparked.utils.GetParkedGlobalUtils;

import static com.parse.getparked.utils.GetParkedGlobalUtils.REGISTRATION_TABLE;

public class AuthActivity extends AppCompatActivity {

    private EditText emailField, passwrodField;
    private ProgressDialog dialog;
    private String emailID;
    private String passwordText;
    private Context activity;
    private boolean isDataThere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        activity = this;
        dialog = new ProgressDialog(AuthActivity.this);
        dialog.setCancelable(false);
        emailField = (EditText) findViewById(R.id.emailField);
        passwrodField = (EditText) findViewById(R.id.passwrodField);
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRegisterStart();
            }
        });
        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginStart();
            }
        });
    }

    public void onLoginStart() {
        showLoading("Logging in...");
        passwordText = passwrodField.getText().toString();
        emailID = emailField.getText().toString();
        if (isValidEmail(emailID) && passwordText.length() > 6) {
            checkUser();
        } else {
            hideProgressDialog();
            Toast.makeText(this, "Please enter valid Email Id and Password.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onRegisterStart() {
        Intent homeIntent = new Intent(AuthActivity.this, RegisterActivity.class);
        startActivity(homeIntent);
    }

    private void checkUser() {
        DatabaseReference newRef = FirebaseDatabase.getInstance().getReference(REGISTRATION_TABLE);
        newRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop:
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    GetParkedRegisterBean post = child.getValue(GetParkedRegisterBean.class);
                    if (post.getEmail().equals(emailID)) {
                        isDataThere = true;
                        if (post.getPassword().equals(passwordText)) {
                            try {
                                GetParkedGlobalUtils.setUserLogin(activity);
                                GetParkedGlobalUtils.setUserId(activity, post.getUserID());
                                Intent homeIntent = new Intent(AuthActivity.this, MainActivity.class);
                                startActivity(homeIntent);
                                finish();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                        break;
                    } else {
                        isDataThere = false;
                    }
                }
                if (!isDataThere)
                    Toast.makeText(activity, "invalid username/password", Toast.LENGTH_SHORT).show();
                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void showLoading(String message) {
        dialog.setMessage(message);
        if ((dialog != null))
            dialog.show();
    }

    private void hideProgressDialog() {
        if ((dialog != null) && (dialog.isShowing())) {
            dialog.dismiss();
        }
    }
}
