package com.parse.getparked.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Vishal.Pandey on 9/11/2017.
 */

public class GetParkedGlobalUtils {
    public static String REGISTRATION_TABLE = "get_parked_registration";
    public static String PARKING_INFO_TABLE = "parking_info";
    public static String PREF = "pref";
    public static String IS_LOGGED_IN = "isLoggedIn";
    public static String USER_ID = "userID";

    public static DatabaseReference getRegistrationTableForPush() {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference newRef = myRef.child(REGISTRATION_TABLE).push();
        return newRef;
    }

    public static DatabaseReference getParkingInfoTableForPush() {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference newRef = myRef.child(PARKING_INFO_TABLE).push();
        return newRef;
    }

    public static void setUserLogin(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF, Context.MODE_PRIVATE).edit();
        editor.putBoolean(IS_LOGGED_IN, true).commit();
    }

    public static void setUserLogout(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF, Context.MODE_PRIVATE).edit();
        editor.putBoolean(IS_LOGGED_IN, false).commit();
    }

    public static boolean isUserLoggedIn(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return preferences.getBoolean(IS_LOGGED_IN, false);
    }

    public static void setUserId(Context context, String userID) {
        SharedPreferences.Editor preferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE).edit();
        preferences.putString(USER_ID, userID).commit();
    }

    public static String getUserId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        return preferences.getString(USER_ID, "");
    }
}
