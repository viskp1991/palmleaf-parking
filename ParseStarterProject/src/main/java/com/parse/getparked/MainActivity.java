/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.getparked;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.getparked.utils.GetParkedGlobalUtils;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    ListView listView;

    ArrayList<ParseObject> itemsArray;

    ItemsListViewAdapter itemsListViewAdapter;
    private ProgressDialog dialog;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dialog = new ProgressDialog(MainActivity.this);
        dialog.setCancelable(false);
        listView = (ListView) findViewById(R.id.listView);
        activity = this;
        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        itemsArray = new ArrayList<>();

        itemsListViewAdapter = new ItemsListViewAdapter(this);

        listView.setAdapter(itemsListViewAdapter);

//        reloadParkingSpaces();
        findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetParkedGlobalUtils.setUserLogout(activity);
                startActivity(new Intent(activity, StartUpActivity.class));
                finish();
            }
        });
    }

    private void reloadParkingSpaces() {
        showLoading("Getting Spaces...");

        ParseQuery<ParseObject> query = ParseQuery.getQuery("ParkItem");

        ParseUser parseUser = ParseUser.getCurrentUser();

//    query.whereEqualTo("UserId", parseUser.getObjectId());

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, com.parse.ParseException e) {
                if (e == null) {
                    itemsArray = (ArrayList) objects;
                    itemsListViewAdapter.notifyDataSetChanged();
                } else {
                    Log.e("score", "Error: " + e.getMessage());
                }
                hideProgressDialog();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_create) {
            Intent intent = new Intent(this, ParkSpaceActivity.class);
            startActivity(intent);
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_show) {
            Intent intent = new Intent(this, SpacesActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private class ItemsListViewAdapter extends BaseAdapter {

        Context rootContext;

        public ItemsListViewAdapter(Context context) {
            super();
            rootContext = context;
        }

        @Override
        public int getCount() {
            return itemsArray.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) rootContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.cell_spaces, parent, false);
            }

            TextView nameView = (TextView) convertView.findViewById(R.id.titleView);

            ParseObject parseObject = itemsArray.get(position);

            nameView.setText("Parking " + (position + 1) + "\n\nParking Start Time - " + parseObject.getDate("StartTime") + "");

            return convertView;
        }
    }

    private void showLoading(String message) {
        dialog.setMessage(message);
        if ((dialog != null))
            dialog.show();
    }

    private void hideProgressDialog() {
        if ((dialog != null) && (dialog.isShowing())) {
            dialog.dismiss();
        }
    }
}
