package com.parse.getparked;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.getparked.pojo.ParkingSpaceInfoPojo;
import com.parse.getparked.utils.GetParkedGlobalUtils;

import java.util.ArrayList;
import java.util.List;

import static com.parse.getparked.utils.GetParkedGlobalUtils.PARKING_INFO_TABLE;

public class SpacesActivity extends AppCompatActivity {

    ListView listView;
    ItemsListViewAdapter listViewAdapter;

    ArrayList<ParkingSpaceInfoPojo> itemsArray;
    private Context activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spaces);
        activity = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        itemsArray = new ArrayList<>();

        listView = (ListView) findViewById(R.id.listView);
        listViewAdapter = new ItemsListViewAdapter(this);

        listView.setAdapter(listViewAdapter);

        reloadParkingSpaces();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            reloadParkingSpaces();
        }
    }

    private void reloadParkingSpaces() {
        DatabaseReference newRef = FirebaseDatabase.getInstance().getReference(PARKING_INFO_TABLE);
        newRef.orderByChild("userId").equalTo(GetParkedGlobalUtils.getUserId(activity)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    ParkingSpaceInfoPojo info = child.getValue(ParkingSpaceInfoPojo.class);
                    itemsArray.add(info);
                }
                listViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private class ItemsListViewAdapter extends BaseAdapter {

        Context rootContext;

        public ItemsListViewAdapter(Context context) {
            super();
            rootContext = context;
        }

        @Override
        public int getCount() {
            return itemsArray.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) rootContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.cell_spaces, parent, false);
            }

            TextView nameView = (TextView) convertView.findViewById(R.id.titleView);

            ParkingSpaceInfoPojo parseObject = itemsArray.get(position);

            nameView.setText(parseObject.getSpaceName());

            return convertView;
        }
    }

}
