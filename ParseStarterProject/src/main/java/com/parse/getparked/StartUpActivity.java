package com.parse.getparked;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.parse.ParseUser;
import com.parse.getparked.UserAuth.AuthActivity;
import com.parse.getparked.utils.GetParkedGlobalUtils;

public class StartUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);
        if (!GetParkedGlobalUtils.isUserLoggedIn(this)) {
            Intent loginIntent = new Intent(this, AuthActivity.class);
            startActivity(loginIntent);
        } else {
            Intent mainIntent = new Intent(this, MainActivity.class);
            startActivity(mainIntent);
        }

        finish();

    }
}
