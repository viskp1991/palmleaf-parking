package com.parse.getparked;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.getparked.UserAuth.AuthActivity;
import com.parse.getparked.UserAuth.RegisterActivity;
import com.parse.getparked.pojo.GetParkedRegisterBean;
import com.parse.getparked.pojo.ParkingSpaceInfoPojo;
import com.parse.getparked.utils.GetParkedGlobalUtils;

public class ParkSpaceActivity extends AppCompatActivity {

    int PLACE_PICKER_REQUEST = 1;

    EditText nameField, rateField, spaceAreaField, spaceCountField, valetAreaField, valetCountField;

    double longitude, lattitude;

    Button locationBtn;
    private ProgressDialog dialog;
    private Context activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_park_space);
        activity = this;
        dialog = new ProgressDialog(ParkSpaceActivity.this);
        dialog.setCancelable(false);
        nameField = (EditText) findViewById(R.id.nameField);
        rateField = (EditText) findViewById(R.id.rateField);
        spaceAreaField = (EditText) findViewById(R.id.spaceAreaField);
        spaceCountField = (EditText) findViewById(R.id.spaceCountField);
        valetAreaField = (EditText) findViewById(R.id.valetAreaField);
        valetCountField = (EditText) findViewById(R.id.valetCountField);

        locationBtn = (Button) findViewById(R.id.locationBtn);
        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLocation();
            }
        });
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancel();
            }
        });
        findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewSpace();
            }
        });
    }

    public void setLocation() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();

                longitude = place.getLatLng().longitude;
                lattitude = place.getLatLng().latitude;

                locationBtn.setText(longitude + " , " + lattitude);

            }
        }
    }

    public void addNewSpace() {
        showLoading("Adding Space...");
        String nameText = nameField.getText().toString().trim();
        String rateText = rateField.getText().toString().trim().trim();
        String spaceAreaText = spaceAreaField.getText().toString().trim();
        String spaceCountText = spaceCountField.getText().toString().trim();
        String valetAreaText = valetAreaField.getText().toString().trim();
        String valetCountText = valetCountField.getText().toString().trim();

        if (nameText.length() > 0 && rateText.length() > 0 && spaceAreaText.length() > 0 && spaceCountText.length() > 0
                && valetAreaText.length() > 0 && valetCountText.length() > 0 && !locationBtn.getText().toString().equals("Set Location")) {
            ParkingSpaceInfoPojo infoPojo = new ParkingSpaceInfoPojo();
            infoPojo.setSpaceName(nameText);
            infoPojo.setRate(Double.valueOf(rateText));
            infoPojo.setSpaceCount(Integer.valueOf(spaceCountText));
            infoPojo.setArea(Integer.valueOf(spaceAreaText));
            infoPojo.setValetCount(Integer.valueOf(valetCountText));
            infoPojo.setValetArea(Integer.valueOf(valetAreaText));
            infoPojo.setUserId(GetParkedGlobalUtils.getUserId(activity));
            infoPojo.setLocation(lattitude + "," + longitude);

            DatabaseReference newRef = GetParkedGlobalUtils.getParkingInfoTableForPush();
            newRef.setValue(infoPojo, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ParkSpaceActivity.this, "Added Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    hideProgressDialog();
                }
            });
        } else {
            Toast.makeText(this, "Please enter all values", Toast.LENGTH_SHORT).show();
        }
    }

    public void onCancel() {
        finish();
    }

    private void showLoading(String message) {
        dialog.setMessage(message);
        if ((dialog != null))
            dialog.show();
    }

    private void hideProgressDialog() {
        if ((dialog != null) && (dialog.isShowing())) {
            dialog.dismiss();
        }
    }
}
